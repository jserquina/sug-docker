/** variable declarations **/
DECLARE @username nvarchar(25) = N'cf_agent';
DECLARE @pwd nvarchar(25) = 'my_super_secret_pwd';
DECLARE @sql varchar(1000) = 'CREATE LOGIN [' + @username + '] WITH PASSWORD = N''' + @pwd +  ''', CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF';
DECLARE @pUser nvarchar(25) = N'cf_agent_user';
DECLARE @dbOwner nvarchar(25) = 'db_owner';
DECLARE @sqlUser varchar(500) = 'CREATE USER [' + @pUser + '] FOR LOGIN [' + @username + ']';
DECLARE @spRoleMember varchar(250) = 'EXEC sp_addrolemember N''' + @dbOwner + ''', N''' + @pUser + '''';

/** create db login account **/
If not Exists (select loginname from master.dbo.syslogins where name = @username)
Begin
	EXECUTE (@sql)
End;

/** create user and associate to login and database - update the database names when applicable **/
Use geniusLocal;
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @pUser )
BEGIN
   EXECUTE (@sqlUser)
   EXECUTE (@spRoleMember)
END;

Use geniusReportLocal;
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @pUser )
BEGIN
	EXECUTE (@sqlUser)
	EXECUTE (@spRoleMember)
END;

Use geniusArchiveLocal;
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @pUser )
BEGIN
	EXECUTE (@sqlUser)
	EXECUTE (@spRoleMember)
END

Use geniusMetricReports;
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = @pUser )
BEGIN
	EXECUTE (@sqlUser)
	EXECUTE (@spRoleMember)
END
