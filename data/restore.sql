/** restore the databases **/
RESTORE DATABASE geniusLocal FROM DISK='/var/opt/sqlserver/main.bak'
    WITH MOVE 'genius' TO '/var/opt/mssql/data/geniusLocal_data.mdf',
    MOVE 'genius_log' TO '/var/opt/mssql/data/geniusLocal_Log.ldf',
    RECOVERY, REPLACE, STATS=10;

RESTORE DATABASE geniusReportLocal FROM DISK='/var/opt/sqlserver/report.bak'
    WITH MOVE 'geniusReport' TO '/var/opt/mssql/data/geniusReportLocal_data.mdf',
    MOVE 'geniusReport_log' TO '/var/opt/mssql/data/geniusReportLocal_Log.ldf',
    RECOVERY, REPLACE, STATS=10;

RESTORE DATABASE geniusArchiveLocal FROM DISK='/var/opt/sqlserver/archive.bak'
    WITH MOVE 'geniusArchive' TO '/var/opt/mssql/data/geniusArchiveLocal_data.mdf',
    MOVE 'geniusArchive_log' TO '/var/opt/mssql/data/geniusArchiveLocal_Log.ldf',
    RECOVERY, REPLACE, STATS=10;

RESTORE DATABASE geniusMetricReports FROM DISK='/var/opt/sqlserver/metric.bak'
    WITH MOVE 'geniusMetricReports' TO '/var/opt/mssql/data/geniusMetricReports_data.mdf',
    MOVE 'geniusMetricReports_log' TO '/var/opt/mssql/data/geniusMetricReports_Log.ldf',
    RECOVERY, REPLACE, STATS=10;
